/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <wayland-client.h>
#include <wayfire/output.hpp>
#include <wayfire/core.hpp>
#include <wayfire/output-layout.hpp>

#include "Hotspot.hpp"

static void handle_hotspot_destroy( wl_resource *resource );

/**
 * Represents a zdesq_shell_hotspot_v1.
 * Lifetime is managed by the resource.
 */
void DesQ::Shell::Hotspot::init_connections() {
    on_tablet_axis =
        [ = ] (auto){
            idle_check_input.run_once(
                [ = ] () {
                    auto gcf = wf::get_core().get_cursor_position();
                    wf::point_t gc { (int)gcf.x, (int)gcf.y };
                    process_input_motion( gc );
                }
            );
        };

    on_motion_event =
        [ = ] (auto){
            idle_check_input.run_once(
                [ = ] () {
                    auto gcf = wf::get_core().get_cursor_position();
                    wf::point_t gc { (int)gcf.x, (int)gcf.y };
                    process_input_motion( gc );
                }
            );
        };

    on_touch_motion =
        [ = ] (auto) {
            idle_check_input.run_once(
                [ = ] () {
                    auto gcf = wf::get_core().get_touch_position( 0 );
                    wf::point_t gc { (int)gcf.x, (int)gcf.y };
                    process_input_motion( gc );
                }
            );
        };
}


void DesQ::Shell::Hotspot::process_input_motion( wf::point_t gc ) {
    if ( !(hotspot_geometry & gc) ) {
        if ( hotspot_triggered ) {
            zdesq_hotspot_v1_send_leave( hotspot_resource );
        }

        /* Cursor outside of the hotspot */
        hotspot_triggered = false;
        timer.disconnect();

        return;
    }

    if ( hotspot_triggered ) {
        /* Hotspot was already triggered, wait for the next time the cursor
         * enters the hotspot area to trigger again */
        return;
    }

    if ( !timer.is_connected() ) {
        timer.set_timeout(
            timeout_ms, [ = ] () {
                hotspot_triggered = true;
                zdesq_hotspot_v1_send_enter( hotspot_resource );
                return false;
            }
        );
    }
}


wf::geometry_t DesQ::Shell::Hotspot::calculate_hotspot_geometry( wf::output_t *output, uint32_t edge_mask, uint32_t distance ) const {
    wf::geometry_t slot = output->get_layout_geometry();

    if ( edge_mask & ZDESQ_OUTPUT_V1_HOTSPOT_EDGE_TOP ) {
        slot.height = distance;
    }

    else if ( edge_mask & ZDESQ_OUTPUT_V1_HOTSPOT_EDGE_BOTTOM ) {
        slot.y     += slot.height - distance;
        slot.height = distance;
    }

    if ( edge_mask & ZDESQ_OUTPUT_V1_HOTSPOT_EDGE_LEFT ) {
        slot.width = distance;
    }

    else if ( edge_mask & ZDESQ_OUTPUT_V1_HOTSPOT_EDGE_RIGHT ) {
        slot.x    += slot.width - distance;
        slot.width = distance;
    }

    return slot;
}


DesQ::Shell::Hotspot::Hotspot( wf::output_t *output, uint32_t edge_mask, uint32_t distance, uint32_t timeout, wl_client *client, uint32_t id ) {
    init_connections();

    this->timeout_ms       = timeout;
    this->hotspot_geometry = calculate_hotspot_geometry( output, edge_mask, distance );

    hotspot_resource = wl_resource_create( client, &zdesq_hotspot_v1_interface, 1, id );
    wl_resource_set_implementation( hotspot_resource, NULL, this, handle_hotspot_destroy );

    // setup output destroy listener
    on_output_removed.set_callback(
        [ this, output ] (wf::output_removed_signal *ev){
            if ( ev->output == output ) {
                /* Make hotspot inactive by setting the region to empty
                 */
                hotspot_geometry = { 0, 0, 0, 0 };
                process_input_motion( { 0, 0 } );
            }
        }
    );

    wf::get_core().connect( &on_motion_event );
    wf::get_core().connect( &on_touch_motion );
    wf::get_core().connect( &on_tablet_axis );
    wf::get_core().output_layout->connect( &on_output_removed );
}


static void handle_hotspot_destroy( wl_resource *resource ) {
    auto *hotspot = (DesQ::Shell::Hotspot *)wl_resource_get_user_data( resource );

    delete hotspot;

    wl_resource_set_user_data( resource, nullptr );
}
