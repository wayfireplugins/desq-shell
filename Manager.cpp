/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <wayfire/core.hpp>
#include <wayfire/seat.hpp>
#include <wayfire/output.hpp>
#include <wayfire/workarea.hpp>
#include <wayfire/output-layout.hpp>
#include <wayfire/workspace-set.hpp>

#include "Output.hpp"
#include "Surface.hpp"
#include "Manager.hpp"

DesQ::Shell::Manager::Manager( wl_display *display ) {
    /** Create the global object */
    manager = wl_global_create( display, &zdesq_shell_manager_v1_interface, 1, this, DesQ::Shell::Manager::bind );

    if ( manager == NULL ) {
        LOGE( "Failed to create desq_shell interface" );
        return;
    }

    placeDropdownOnMapped =
        [ = ] (wf::view_mapped_signal *ev) {
            /** Get the available geometry of the current workspace of the current output */
            auto workarea = ev->view->get_output()->workarea->get_workarea();

            /** Get the geometry of the view */
            auto window = wf::toplevel_cast( ev->view )->get_geometry();

            /** Get the horizontal center */
            window.x = workarea.x + (workarea.width / 2) - (window.width / 2);

            /** Top of the current workspace */
            window.y = workarea.y;

            /** Move the view */
            wf::toplevel_cast( ev->view )->move( window.x, window.y );

            ev->is_positioned = true;
        };
}


DesQ::Shell::Manager::~Manager() {
    if ( manager ) {
        wl_global_destroy( manager );
        manager = nullptr;
    }
}


void DesQ::Shell::Manager::bind( wl_client *client, void *data, uint32_t version, uint32_t id ) {
    DesQ::Shell::Manager *Mgr = (DesQ::Shell::Manager *)data;

    Mgr->resource = wl_resource_create( client, &zdesq_shell_manager_v1_interface, version, id );

    if ( Mgr->resource == nullptr ) {
        wl_client_post_no_memory( client );
        return;
    }

    wl_resource_set_implementation( Mgr->resource, &zdesq_shell_manager_v1_impl, Mgr, nullptr );
}


void DesQ::Shell::Manager::handleGetDesQOutputRequest( wl_client *client, wl_resource *, wl_resource *output, uint32_t id ) {
    auto wlr_out = (wlr_output *)wl_resource_get_user_data( output );
    auto wo      = wf::get_core().output_layout->find_output( wlr_out );

    if ( wo ) {
        // will be deleted when the resource is destroyed
        new DesQ::Shell::Output( wo, client, id );
    }
}


void DesQ::Shell::Manager::handleGetDesQSurfaceRequest( wl_client *client, wl_resource *, wl_resource *surface, uint32_t id ) {
    auto view = wf::wl_surface_to_wayfire_view( surface );

    if ( view ) {
        /* Will be freed when the resource is destroyed */
        new DesQ::Shell::Surface( view, client, id );
    }
}


void DesQ::Shell::Manager::handleSetBackgroundRequest( wl_client *, wl_resource *resource, wl_resource *surface, wl_resource *output ) {
    auto Manager = (DesQ::Shell::Manager *)wl_resource_get_user_data( resource );

    wlr_output   *wlr_out = nullptr;
    wf::output_t *wf_out  = nullptr;

    if ( output ) {
        wlr_out = (wlr_output *)wl_resource_get_user_data( output );
        wf_out  = wf::get_core().output_layout->find_output( wlr_out );
    }

    else {
        wf_out = wf::get_core().seat->get_active_output();
    }

    auto view = wf::toplevel_cast( wf::wl_surface_to_wayfire_view( surface ) );

    if ( (view != nullptr) and (wf_out != nullptr) ) {
        /** Close the existing view if it's set */
        if ( Manager->mBackgrounds[ wf_out ].view != nullptr ) {
            Manager->mBackgrounds[ wf_out ].view->close();
        }

        // view->set_decoration( nullptr );
        // wf::move_view_to_output( view, wf_out, false );
        // view->set_geometry( wf_out->get_relative_geometry() );
        // wf_out->workspace->add_view( view, wf::LAYER_BACKGROUND );
        // view->sticky = true;
        // view->set_role( wf::VIEW_ROLE_DESKTOP_ENVIRONMENT );

        // Manager->mBackgrounds[ wf_out ].view = view;
    }
}


void DesQ::Shell::Manager::handleSetDropdownRequest( wl_client *, wl_resource *resource, wl_resource *output, wl_resource *surface ) {
    auto Manager = (DesQ::Shell::Manager *)wl_resource_get_user_data( resource );

    wlr_output   *wlr_out = nullptr;
    wf::output_t *wf_out  = nullptr;

    if ( output ) {
        wlr_out = (wlr_output *)wl_resource_get_user_data( output );
        wf_out  = wf::get_core().output_layout->find_output( wlr_out );
    }

    else {
        wf_out = wf::get_core().seat->get_active_output();
    }

    auto view = wf::toplevel_cast( wf::wl_surface_to_wayfire_view( surface ) );

    // if ( (view != nullptr) and (wf_out != nullptr) ) {
    //     view->set_decoration( nullptr );
    //     wf::get_core().move_view_to_output( view, wf_out, false );
    //     view->set_geometry( wf_out->get_relative_geometry() );
    //
    //     wf_out->workspace->add_view( view, wf::LAYER_TOP );
    //     view->sticky = true;
    //     view->set_role( wf::VIEW_ROLE_DESKTOP_ENVIRONMENT );
    //
    //     /** Place it properly every time it's shown */
    //     view->connect( &Manager->placeDropdownOnMapped );
    // }
}


void DesQ::Shell::Manager::handleCreatePanelRequest( wl_client *, wl_resource *, wl_resource *, wl_resource *, uint32_t ) {
}


struct zdesq_shell_manager_v1_interface DesQ::Shell::Manager::zdesq_shell_manager_v1_impl = {
    .get_desq_output  = handleGetDesQOutputRequest,
    .get_desq_surface = handleGetDesQSurfaceRequest,
    .set_background   = handleSetBackgroundRequest,
    .set_dropdown     = handleSetDropdownRequest,
    .create_panel     = handleCreatePanelRequest,
};
