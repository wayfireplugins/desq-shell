/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <wayland-client.h>
#include <wayfire/signal-definitions.hpp>
#include <wayfire/nonstd/wlroots-full.hpp>

#include "desq-shell-unstable-v1-protocol.h"

/**
 * Represents a zdesq_shell_hotspot_v1.
 * Lifetime is managed by the resource.
 */

namespace DesQ {
    namespace Shell {
        class Hotspot;
    }
}

class DesQ::Shell::Hotspot {
    private:
        wf::geometry_t hotspot_geometry;

        bool hotspot_triggered = false;
        wf::wl_idle_call idle_check_input;
        wf::wl_timer<false> timer;

        uint32_t timeout_ms;
        wl_resource *hotspot_resource;

        void init_connections();

        wf::signal::connection_t<wf::post_input_event_signal<wlr_tablet_tool_axis_event> > on_tablet_axis;
        wf::signal::connection_t<wf::post_input_event_signal<wlr_pointer_motion_event> > on_motion_event;
        wf::signal::connection_t<wf::post_input_event_signal<wlr_touch_motion_event> > on_touch_motion;
        wf::signal::connection_t<wf::output_removed_signal> on_output_removed;

        void process_input_motion( wf::point_t gc );

        wf::geometry_t calculate_hotspot_geometry( wf::output_t *output, uint32_t edge_mask, uint32_t distance ) const;

        Hotspot( const DesQ::Shell::Hotspot& )             = delete;
        Hotspot( DesQ::Shell::Hotspot&& )                  = delete;
        Hotspot& operator =( const DesQ::Shell::Hotspot& ) = delete;
        Hotspot& operator =( DesQ::Shell::Hotspot&& )      = delete;

    public:

        /**
         * Create a new hotspot.
         * It is guaranteedd that edge_mask contains at most 2 non-opposing edges.
         */
        Hotspot( wf::output_t *output, uint32_t edge_mask, uint32_t distance, uint32_t timeout, wl_client *client, uint32_t id );
        ~Hotspot() = default;
};
