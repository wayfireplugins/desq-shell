/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <wayfire/output.hpp>
#include <wayfire/core.hpp>
#include <wayfire/output-layout.hpp>
#include <wayfire/render-manager.hpp>
#include <wayfire/workspace-set.hpp>

#include "Surface.hpp"

static void handle_surface_destroy( wl_resource *resource );
static void handle_zdesq_surface_interactive_move( wl_client *, wl_resource *resource );

static struct zdesq_surface_v1_interface zdesq_surface_impl = {
    .interactive_move = handle_zdesq_surface_interactive_move,
};

DesQ::Shell::Surface::Surface( wayfire_view view, wl_client *client, int id ) {
    on_unmap =
        [ = ] (auto) mutable {
            view = nullptr;
        };

    this->view = view;
    resource   = wl_resource_create( client, &zdesq_surface_v1_interface, 1, id );
    wl_resource_set_implementation( resource, &zdesq_surface_impl, this, handle_surface_destroy );
    view->connect( &on_unmap );
}


void DesQ::Shell::Surface::interactive_move() {
    // if ( view ) {
    //     view->move_request();
    // }
}


static void handle_zdesq_surface_interactive_move( wl_client *, wl_resource *resource ) {
    auto surface = (DesQ::Shell::Surface *)wl_resource_get_user_data( resource );

    surface->interactive_move();
}


static void handle_surface_destroy( wl_resource *resource ) {
    auto surface = (DesQ::Shell::Surface *)wl_resource_get_user_data( resource );

    delete surface;
    wl_resource_set_user_data( resource, nullptr );
}
