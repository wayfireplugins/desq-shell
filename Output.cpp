/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <wayland-client.h>
#include <wayfire/output.hpp>
#include <wayfire/core.hpp>
#include <wayfire/output-layout.hpp>
#include <wayfire/render-manager.hpp>
#include <wayfire/workspace-set.hpp>

#include "Output.hpp"
#include "Hotspot.hpp"

DesQ::Shell::Output::Output( wf::output_t *output, wl_client *client, int id ) {
    /** Store the current output */
    this->output = output;

    /** Create signal callbacks */
    init_connections();

    /** Create the wayland resource */
    resource = wl_resource_create( client, &zdesq_output_v1_interface, 1, id );

    /** Set the resource implementation */
    wl_resource_set_implementation( resource, &zdesq_output_impl, this, &DesQ::Shell::Output::handle_output_destroy );

    /**
     * We're done binding the resource to it's implementation.
     * Send various events
     */

    wf::dimensions_t grid  = output->wset()->get_workspace_grid_size();
    wf::point_t      curWS = output->wset()->get_current_workspace();

    /** height represents the rows, width represents the columns */
    zdesq_output_v1_send_workspace_extents( resource, grid.height, grid.width );

    /** y represents the row, x represents the column */
    zdesq_output_v1_send_active_workspace( resource, curWS.y, curWS.x );

    /** Send info about views in this output */
    listAllOutputViews();

    /** We're done sending the events */
    zdesq_output_v1_send_done( resource );

    /** Conect the signal callbacks: Core */
    wf::get_core().output_layout->connect( &on_output_removed );
    wf::get_core().connect( &on_view_set_output );
    wf::get_core().connect( &on_view_moved_to_wset );

    /** Conect the signal callbacks: Current output */
    output->connect( &on_fullscreen_layer_focused );
    output->connect( &on_workspace_grid_changed );
    output->connect( &on_workspace_changed );
    output->connect( &on_view_workspace_changed );
    output->connect( &on_view_sticky_changed );
}


DesQ::Shell::Output::~Output() {
    if ( !this->output ) {
        /* The wayfire output was destroyed. Gracefully do nothing */
        return;
    }

    disconnect_from_output();

    /* Remove any remaining inhibits, otherwise the compositor will never be "unlocked" */
    while ( num_inhibits > 0 ) {
        this->output->render->add_inhibit( false );
        --num_inhibits;
    }
}


void DesQ::Shell::Output::inhibit_output() {
    ++this->num_inhibits;

    if ( this->output ) {
        this->output->render->add_inhibit( true );
    }
}


void DesQ::Shell::Output::inhibit_output_done() {
    if ( this->num_inhibits == 0 ) {
        wl_resource_post_no_memory( resource );

        return;
    }

    --this->num_inhibits;

    if ( this->output ) {
        this->output->render->add_inhibit( false );
    }
}


void DesQ::Shell::Output::create_hotspot( uint32_t hotspot, uint32_t threshold, uint32_t timeout, uint32_t id ) {
    // will be auto-deleted when the resource is destroyed by the client
    new DesQ::Shell::Hotspot( this->output, hotspot, threshold, timeout, wl_resource_get_client( this->resource ), id );
}


void DesQ::Shell::Output::listAllOutputViews() {
    wf::dimensions_t grid = output->wset()->get_workspace_grid_size();

    for ( wayfire_toplevel_view view: output->wset()->get_views( wf::WSET_MAPPED_ONLY | wf::WSET_SORT_STACKING ) ) {
        /** We don't care for non-toplevel views */
        if ( view->role != wf::VIEW_ROLE_TOPLEVEL ) {
            continue;
        }

        /** If the view is sticky, send view_added signal for all workspaces */
        if ( view->sticky ) {
            for ( int r = 0; r < grid.height; r++ ) {
                for ( int c = 0; c < grid.width; c++ ) {
                    zdesq_output_v1_send_view_added( resource, view->get_id(), r, c );
                }
            }
        }

        /** The view is mainly on one workspace */
        else {
            wf::point_t ws = output->wset()->get_view_main_workspace( view );
            zdesq_output_v1_send_view_added( resource, view->get_id(), ws.y, ws.x );
        }
    }
}


void DesQ::Shell::Output::disconnect_from_output() {
    wf::get_core().output_layout->disconnect( &on_output_removed );
    on_fullscreen_layer_focused.disconnect();
}


void DesQ::Shell::Output::init_connections() {
    on_output_removed =
        [ = ] (wf::output_removed_signal *ev){
            if ( ev->output == this->output ) {
                disconnect_from_output();
                this->output = nullptr;
            }
        };

    on_fullscreen_layer_focused =
        [ = ] (wf::fullscreen_layer_focused_signal *ev){
            if ( ev->has_promoted ) {
                zdesq_output_v1_send_enter_fullscreen( resource );
            }

            else {
                zdesq_output_v1_send_leave_fullscreen( resource );
            }
        };

    on_workspace_grid_changed =
        [ = ] (wf::workspace_grid_changed_signal *ev) {
            wf::point_t curWS = output->wset()->get_current_workspace();

            /** Send the workspace_extents signal */
            zdesq_output_v1_send_workspace_extents( resource, ev->new_grid_size.height, ev->new_grid_size.width );

            /** Send the active_workspace signal, because the active workspace might have changed */
            zdesq_output_v1_send_active_workspace( resource, curWS.y, curWS.x );

            /** We're done sending the events */
            zdesq_output_v1_send_done( resource );
        };

    on_workspace_changed =
        [ = ] (wf::workspace_changed_signal *ev) {
            /** Send the active_workspace signal */
            zdesq_output_v1_send_active_workspace( resource, ev->new_viewport.y, ev->new_viewport.x );

            /** We're done sending the events */
            zdesq_output_v1_send_done( resource );
        };

    on_view_workspace_changed =
        [ = ] (wf::view_change_workspace_signal *ev) {
            if ( ev->view->role != wf::VIEW_ROLE_TOPLEVEL ) {
                return;
            }

            /** Moved from one to another */
            if ( ev->old_workspace_valid ) {
                zdesq_output_v1_send_view_removed( resource, ev->view->get_id(), ev->from.y, ev->from.x );
            }

            else {
                zdesq_output_v1_send_view_added( resource, ev->view->get_id(), ev->to.y, ev->to.x );
            }
        };

    on_view_set_output =
        [ = ](wf::view_set_output_signal *ev) {
            if ( ev->view->role != wf::VIEW_ROLE_TOPLEVEL ) {
                return;
            }

            /** Check the view's output is our output */
            if ( ev->view->get_output() != output ) {
                return;
            }

            /** Get the current workspace */
            wf::point_t curWS = output->wset()->get_current_workspace();
            zdesq_output_v1_send_view_added( resource, ev->view->get_id(), curWS.y, curWS.x );
        };

    on_view_moved_to_wset =
        [ = ](wf::view_moved_to_wset_signal *ev) {
            if ( ev->view->role != wf::VIEW_ROLE_TOPLEVEL ) {
                return;
            }

            /** If the old_output was this output */
            if ( ev->old_wset && ev->old_wset->get_attached_output() == output ) {
                wf::point_t ws = output->wset()->get_current_workspace();
                zdesq_output_v1_send_view_removed( resource, ev->view->get_id(), ws.y, ws.x );
            }

            /** If the new_output is this output */
            if ( ev->new_wset && ev->new_wset->get_attached_output() == output ) {
                /** Get the current workspace */
                wf::point_t curWS = output->wset()->get_current_workspace();
                zdesq_output_v1_send_view_added( resource, ev->view->get_id(), curWS.y, curWS.x );
            }
        };

    on_view_sticky_changed =
        [ = ](wf::view_set_sticky_signal *ev) {
            if ( ev->view->role != wf::VIEW_ROLE_TOPLEVEL ) {
                return;
            }

            wf::dimensions_t grid = output->wset()->get_workspace_grid_size();

            /** If the view was made sticky, emit view_added for all workspaces */
            if ( ev->view->sticky ) {
                for ( int r = 0; r < grid.height; r++ ) {
                    for ( int c = 0; c < grid.width; c++ ) {
                        zdesq_output_v1_send_view_added( resource, ev->view->get_id(), r, c );
                    }
                }
            }

            /** If sticky was unset, emit view_removed for all workspaces, except the current one */
            else {
                wf::point_t ws = output->wset()->get_view_main_workspace( ev->view );
                for ( int r = 0; r < grid.height; r++ ) {
                    for ( int c = 0; c < grid.width; c++ ) {
                        if ( ws.y != r and ws.x != c ) {
                            zdesq_output_v1_send_view_removed( resource, ev->view->get_id(), r, c );
                        }
                    }
                }
            }
        };
}


void DesQ::Shell::Output::handle_zdesq_output_inhibit_output( wl_client *, wl_resource *resource ) {
    auto output = (DesQ::Shell::Output *)wl_resource_get_user_data( resource );

    output->inhibit_output();
}


void DesQ::Shell::Output::handle_zdesq_output_inhibit_output_done( wl_client *, wl_resource *resource ) {
    auto output = (DesQ::Shell::Output *)wl_resource_get_user_data( resource );

    output->inhibit_output_done();
}


void DesQ::Shell::Output::handle_zdesq_output_create_hotspot( wl_client *, wl_resource *resource, uint32_t hotspot, uint32_t threshold, uint32_t timeout, uint32_t id ) {
    auto output = (DesQ::Shell::Output *)wl_resource_get_user_data( resource );

    output->create_hotspot( hotspot, threshold, timeout, id );
}


void DesQ::Shell::Output::handle_zdesq_output_get_workspace_extents( wl_client *, wl_resource *resource ) {
    auto output = (DesQ::Shell::Output *)wl_resource_get_user_data( resource );

    wf::dimensions_t grid = output->output->wset()->get_workspace_grid_size();

    /** height represents the rows, width represents the columns */
    zdesq_output_v1_send_workspace_extents( output->resource, grid.height, grid.width );

    /** We're done sending the events */
    zdesq_output_v1_send_done( output->resource );
}


void DesQ::Shell::Output::handle_zdesq_output_get_active_workspace( wl_client *, wl_resource *resource ) {
    auto output = (DesQ::Shell::Output *)wl_resource_get_user_data( resource );

    wf::point_t curWS = output->output->wset()->get_current_workspace();

    /** y represents the row, x represents the column */
    zdesq_output_v1_send_active_workspace( output->resource, curWS.y, curWS.x );

    /** We're done sending the events */
    zdesq_output_v1_send_done( output->resource );
}


void DesQ::Shell::Output::handle_zdesq_output_set_active_workspace( wl_client *, wl_resource *resource, int row, int col ) {
    auto output = (DesQ::Shell::Output *)wl_resource_get_user_data( resource );

    wf::dimensions_t grid = output->output->wset()->get_workspace_grid_size();

    if ( (row < grid.height) or (col < grid.width) ) {
        output->output->wset()->request_workspace( { col, row } );
    }
}


void DesQ::Shell::Output::handle_output_destroy( wl_resource *resource ) {
    auto *output = (DesQ::Shell::Output *)wl_resource_get_user_data( resource );

    delete output;

    wl_resource_set_user_data( resource, nullptr );
}


struct zdesq_output_v1_interface DesQ::Shell::Output::zdesq_output_impl = {
    .inhibit_output        = handle_zdesq_output_inhibit_output,
    .inhibit_output_done   = handle_zdesq_output_inhibit_output_done,
    .create_hotspot        = handle_zdesq_output_create_hotspot,
    .get_workspace_extents = handle_zdesq_output_get_workspace_extents,
    .get_active_workspace  = handle_zdesq_output_get_active_workspace,
    .set_active_workspace  = handle_zdesq_output_set_active_workspace,
};
