/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <wayland-client.h>
#include <wayfire/signal-definitions.hpp>

#include "desq-shell-unstable-v1-protocol.h"

namespace DesQ {
    namespace Shell {
        class Output;
    }
}

/**
 * Represents a zdesq_output_v1.
 * Lifetime is managed by the wl_resource
 */
class DesQ::Shell::Output {
    public:
        Output( wf::output_t *output, wl_client *client, int id );
        ~Output();

        Output( const DesQ::Shell::Output& )             = delete;
        Output( DesQ::Shell::Output&& )                  = delete;
        Output& operator =( const DesQ::Shell::Output& ) = delete;
        Output& operator =( DesQ::Shell::Output&& )      = delete;

        /** Stop rendering this output */
        void inhibit_output();

        /** Resume rendering this output */
        void inhibit_output_done();

        /**
         * Create a hotspot on this output at @hotspot, with an activation region @threshold,
         * time threshold of @timeout.
         */
        void create_hotspot( uint32_t hotspot, uint32_t threshold, uint32_t timeout, uint32_t id );

        /**
         * List all views that belong to this output
         * For each view that belong to this output, a view_added signal will be sent.
         * The view's main workspace will be sent as the workspace. If the view is sticky,
         * a signal is sent for each workspace.
         */
        void listAllOutputViews();

    private:
        /** Disconnect from the underlying output */
        void disconnect_from_output();

        void init_connections();

        /** Interface handlers */
        static void handle_output_destroy( wl_resource *resource );
        static void handle_zdesq_output_inhibit_output( wl_client *, wl_resource *resource );
        static void handle_zdesq_output_inhibit_output_done( wl_client *, wl_resource *resource );
        static void handle_zdesq_output_create_hotspot( wl_client *, wl_resource *resource, uint32_t hotspot, uint32_t threshold, uint32_t timeout, uint32_t id );
        static void handle_zdesq_output_get_workspace_extents( wl_client *, wl_resource *resource );
        static void handle_zdesq_output_get_active_workspace( wl_client *, wl_resource *resource );
        static void handle_zdesq_output_set_active_workspace( wl_client *, wl_resource *resource, int row, int col );

        /** Interface object */
        static struct zdesq_output_v1_interface zdesq_output_impl;

        uint32_t num_inhibits = 0;
        wl_resource *resource;
        wf::output_t *output;

        /** This output was destroyed */
        wf::signal::connection_t<wf::output_removed_signal> on_output_removed;

        /** To know when a view enters/exits fullscreen on this output */
        wf::signal::connection_t<wf::fullscreen_layer_focused_signal> on_fullscreen_layer_focused;

        /** Workspace extents changed */
        wf::signal::connection_t<wf::workspace_grid_changed_signal> on_workspace_grid_changed;

        /** Active workspace changed */
        wf::signal::connection_t<wf::workspace_changed_signal> on_workspace_changed;

        /** A view was moved from one workspace to another */
        wf::signal::connection_t<wf::view_change_workspace_signal> on_view_workspace_changed;

        /** A view was added: check it's output and emit the signal */
        wf::signal::connection_t<wf::view_set_output_signal> on_view_set_output;

        /** A view moved from one output to another */
        wf::signal::connection_t<wf::view_moved_to_wset_signal> on_view_moved_to_wset;

        /** A view was set/unset as sticky. */
        wf::signal::connection_t<wf::view_set_sticky_signal> on_view_sticky_changed;
};
