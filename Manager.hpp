/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <wayland-client.h>

namespace DesQ {
    namespace Shell {
        class Manager;
    }
}

class DesQ::Shell::Manager {
    public:
        Manager( wl_display *display );
        ~Manager();

        static void bind( wl_client *client, void *data, uint32_t version, uint32_t id );

    private:
        static void handleGetDesQOutputRequest( wl_client *client, wl_resource *resource, wl_resource *output, uint32_t id );
        static void handleGetDesQSurfaceRequest( wl_client *client, wl_resource *resource, wl_resource *surface, uint32_t id );
        static void handleSetBackgroundRequest( wl_client *client, wl_resource *resource, wl_resource *surface, wl_resource *output );
        static void handleSetDropdownRequest( wl_client *client, wl_resource *resource, wl_resource *output, wl_resource *surface );
        static void handleCreatePanelRequest( wl_client *client, wl_resource *resource, wl_resource *output, wl_resource *surface, uint32_t id );

        /** Interface object */
        static struct zdesq_shell_manager_v1_interface zdesq_shell_manager_v1_impl;

        /** Global resource */
        wl_global *manager = nullptr;

        /** Internal resource */
        wl_resource *resource = nullptr;

        struct BackgroundView {
            wayfire_toplevel_view view;
        };

        /**  Output <-> Background Map*/
        std::map<wf::output_t *, BackgroundView> mBackgrounds;

        /** An effective way to place a dropdown */
        wf::signal::connection_t<wf::view_mapped_signal> placeDropdownOnMapped;
};
